package be.kdg.se3.herex2018.gen.impl;

import be.kdg.se3.herex2018.gen.api.MessageService;
import be.kdg.se3.herex2018.gen.exceptions.CommunicationException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * A class which implements {@link MessageService} and is specifically implemented for a RabbitMQ message broker.
 */
public class RabbitMQ implements MessageService {
    private final String QUEUENAME;
    private final String CONNECTION_STRING;

    private Connection connection;
    private Channel channel;

    private final Logger logger = LoggerFactory.getLogger(RabbitMQ.class);

    public RabbitMQ(String QUEUENAME, String CONNECTION_STRING) {
        this.QUEUENAME = QUEUENAME;
        this.CONNECTION_STRING = CONNECTION_STRING;
    }

    @Override
    public void init() throws CommunicationException {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(CONNECTION_STRING);

            factory.setRequestedHeartbeat(30);
            factory.setConnectionTimeout(30000);

            connection = factory.newConnection();
            channel = connection.createChannel();

            channel.queueDeclare(QUEUENAME,
                    false,
                    false,
                    false,
                    null);

            logger.info(String.format("The queue has been initialized with name: %s and is connected to %s.", QUEUENAME, CONNECTION_STRING));
        } catch (Exception e) {
            throw new CommunicationException("Something went wrong while in the initialisation of the chanel", e);
        }

    }

    @Override
    public void sendMessage(String message) throws CommunicationException {
        try {
            channel.basicPublish("", QUEUENAME, null, message.getBytes());
            logger.info("A message ha been sent");
            System.out.println(message);
            logger.debug(message);
        } catch (Exception e) {
            throw new CommunicationException("Unable to sent the message");
        }
    }

    @Override
    public void shutdown() throws CommunicationException {
        try {
            channel.close();
            connection.close();
            logger.info("The queue has been shut down");
        } catch (IOException | TimeoutException e) {
            throw new CommunicationException("Unable to close connection to RabbitMQ", e);
        }

    }
}
