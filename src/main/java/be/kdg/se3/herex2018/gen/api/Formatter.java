package be.kdg.se3.herex2018.gen.api;

import be.kdg.se3.herex2018.gen.exceptions.FormatException;

/**
 * An interface to be implemented by any class who will translate an object to a desired format (XNL, JSON, ...).
 */
public interface Formatter {
    /**
     * This method allows an object to be translated to the desired format.
     *
     * @param o a (DTO) object to be translated to the desired format
     * @return the string with the object in the desired format
     * @throws FormatException
     */
    String wrap(Object o) throws FormatException;
}
