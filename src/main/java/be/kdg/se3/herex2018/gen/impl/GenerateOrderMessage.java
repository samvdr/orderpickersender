package be.kdg.se3.herex2018.gen.impl;

import be.kdg.se3.herex2018.gen.api.Formatter;
import be.kdg.se3.herex2018.gen.api.GenerateMessageCommand;
import be.kdg.se3.herex2018.gen.exceptions.FormatException;
import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.gen.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A class responsible for the generating messages to create an order.
 * He implements the {@link GenerateMessageCommand} to ensure he has the right methods.
 */
public class GenerateOrderMessage implements GenerateMessageCommand {
    private List<Integer> customerIds;
    private List<Integer> productIds;
    private int maxAmountOfProducts;
    private int minAmountOfProducts;
    private int maxPrice;
    private int minPrice;
    private int maxProductItemAmount;
    private int minProductItemAmount;
    private Formatter formatter;
    private Random random;

    private final Logger logger = LoggerFactory.getLogger(GenerateOrderMessage.class);

    public GenerateOrderMessage() {
        this.random = new Random();
    }

    public void setCustomerIds(List<Integer> customerIds) {
        this.customerIds = customerIds;
        logger.info("The customer id's have been set");
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
        logger.info("The product id's have been set");
    }

    public void setMaxAmountOfProducts(int maxAmountOfProducts) {
        this.maxAmountOfProducts = maxAmountOfProducts;
        logger.info(String.format("The maximum amount of products has been set to: %d", maxAmountOfProducts));
    }

    public void setMinAmountOfProducts(int minAmountOfProducts) {
        this.minAmountOfProducts = minAmountOfProducts;
        logger.info(String.format("The minimum amount of products has been set to: %d", minAmountOfProducts));
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
        logger.info(String.format("The maximum price for the generated orders has been set to: %d", maxPrice));
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
        logger.info(String.format("The minimum price for the generated orders has been set to: %d", minPrice));
    }

    public void setMaxProductItemAmount(int maxProductItemAmount) {
        this.maxProductItemAmount = maxProductItemAmount;
        logger.info(String.format("The maximum items of a product has been set to: %d", maxProductItemAmount));
    }

    public void setMinProductItemAmount(int minProductItemAmount) {
        this.minProductItemAmount = minProductItemAmount;
        logger.info(String.format("The minimum items of a product has been set to: %d", minProductItemAmount));
    }

    @Override
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
        logger.info(String.format("The formatter has been set to: %s", formatter.getClass().getCanonicalName()));
    }

    @Override
    public String generate(int orderId) throws FormatException {
        ArrayList<Product> items = getProducts();

        OrderMessageDTO orderMessageDTO = new OrderMessageDTO(orderId,
                customerIds.get(random.nextInt(customerIds.size() - 1)),
                random.nextInt(maxPrice - minPrice) + minPrice,
                items);

        String message = formatter.wrap(orderMessageDTO);
        logger.debug(String.format("message content: %s", message));
        return message;
    }

    private ArrayList<Product> getProducts() {
        int amountOfProducts = random.nextInt(maxAmountOfProducts - minAmountOfProducts) + minAmountOfProducts;
        ArrayList<Product> items = new ArrayList<>(amountOfProducts);

        for (int i = 0; i < amountOfProducts; i++) {
            Product product= new Product(customerIds.get(random.nextInt(customerIds.size() - 1)),
                    random.nextInt(maxProductItemAmount - minProductItemAmount) + minProductItemAmount);

            items.add(product);
        }

        return items;
    }
}
