package be.kdg.se3.herex2018.gen;

import be.kdg.se3.herex2018.gen.api.GenerateMessageCommand;
import be.kdg.se3.herex2018.gen.api.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The main generator to generate te messages at the right time.
 */
public class Generator {
    private final MessageGenerator generator;
    private int intervalBetweenOrders;
    private ScheduledExecutorService service;

    private final Logger logger = LoggerFactory.getLogger(Generator.class);

    public Generator(MessageService orderService,
                     MessageService cancelService,
                     GenerateMessageCommand orderCommand,
                     GenerateMessageCommand cancelCommand) {
        this.generator = new MessageGenerator(
                orderService,
                cancelService,
                orderCommand,
                cancelCommand);
        this.intervalBetweenOrders = 500;
        service = Executors.newSingleThreadScheduledExecutor();
    }

    public void setIntervalBetweenOrders(int intervalBetweenOrders) {
        this.intervalBetweenOrders = intervalBetweenOrders;
        logger.info(String.format("The interval between sending ordermessages is set to: %d milliseconds.", intervalBetweenOrders));
    }

    public void setTimeBetweenOrderCancel(int timeBetweenOrderCancel) {
        this.generator.setTimeBetweenOrderCancel(timeBetweenOrderCancel);
        logger.info(String.format("The time between sending an ordermessage and it's cancelmessage is set to: %d milliseconds", timeBetweenOrderCancel));
    }

    public void setCancelAfterXOrders(int cancelAfterXOrders) {
        this.generator.setCancelAfterXOrders(cancelAfterXOrders);
        logger.info(String.format("After %d there will be one cancelled",  cancelAfterXOrders));
    }

    public void start() {
        logger.info("The generator has started.");
        generator.initServices();

        service.scheduleAtFixedRate(generator, 0, Integer.toUnsignedLong(intervalBetweenOrders), TimeUnit.MILLISECONDS);
    }

    public void stop() {
        generator.stop();
        logger.info("The generator has stopped.");
    }
}
