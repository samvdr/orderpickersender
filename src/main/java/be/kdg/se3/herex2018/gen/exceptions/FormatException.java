package be.kdg.se3.herex2018.gen.exceptions;

/**
 * An exception thrown if anything goes wrong in the translating a message to xml, json or any other format.
 */
public class FormatException extends Exception {
    public FormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public FormatException(String message) {
        super(message);
    }
}
