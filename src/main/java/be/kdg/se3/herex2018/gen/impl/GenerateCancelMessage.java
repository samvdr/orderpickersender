package be.kdg.se3.herex2018.gen.impl;

import be.kdg.se3.herex2018.gen.api.Formatter;
import be.kdg.se3.herex2018.gen.api.GenerateMessageCommand;
import be.kdg.se3.herex2018.gen.exceptions.FormatException;
import be.kdg.se3.herex2018.gen.model.CancelMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class responsible for the generating messages to cancel an order.
 * He implements the {@link GenerateMessageCommand} to ensure he has the right methods.
 */
public class GenerateCancelMessage implements GenerateMessageCommand {
    private Formatter formatter;

    private final Logger logger = LoggerFactory.getLogger(GenerateCancelMessage.class);

    @Override
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
        logger.info(String.format("The formatter has been set to: %s", formatter.getClass().getCanonicalName()));
    }

    @Override
    public String generate(int orderId) throws FormatException {
        CancelMessageDTO cancelMessageDTO = new CancelMessageDTO(orderId);

        String message = formatter.wrap(cancelMessageDTO);
        logger.debug(String.format("message content: %s", message));
        return message;
    }
}
