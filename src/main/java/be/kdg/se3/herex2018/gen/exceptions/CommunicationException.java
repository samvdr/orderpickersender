package be.kdg.se3.herex2018.gen.exceptions;

import be.kdg.se3.herex2018.gen.api.MessageService;

/**
 * An exception thrown if anything goes wrong in the communication with the {@link MessageService}.
 */
public class CommunicationException extends Exception {
    public CommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommunicationException(String message) {
        super(message);
    }
}
