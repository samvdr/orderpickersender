package be.kdg.se3.herex2018.gen;

import be.kdg.se3.herex2018.gen.api.GenerateMessageCommand;
import be.kdg.se3.herex2018.gen.api.MessageService;
import be.kdg.se3.herex2018.gen.exceptions.CommunicationException;
import be.kdg.se3.herex2018.gen.exceptions.FormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * A generator implementing the Runnable interface to be able to run in a separate thread and generates a new {@link be.kdg.se3.herex2018.gen.model.OrderMessageDTO} each run.
 * After an adjustable amount of OrderMessages we will scheduale a new tosk to generate a {@link be.kdg.se3.herex2018.gen.model.CancelMessageDTO} for an order.
 */
public class MessageGenerator implements Runnable {
    private final MessageService orderService;
    private final MessageService cancelService;
    private final GenerateMessageCommand orderCommand;
    private final GenerateMessageCommand cancelCommand;
    private int orderId;
    private int timeBetweenOrderCancel;
    private int cancelAfterXOrders;
    private int counter = 0;

    private final Logger logger = LoggerFactory.getLogger(MessageGenerator.class);

    public MessageGenerator(MessageService orderService,
                            MessageService cancelService,
                            GenerateMessageCommand orderCommand,
                            GenerateMessageCommand cancelCommand) {
        this.orderService = orderService;
        this.cancelService = cancelService;
        this.orderCommand = orderCommand;
        this.cancelCommand = cancelCommand;
        this.orderId = 999999;
        this.timeBetweenOrderCancel = 60000;
        this.cancelAfterXOrders = 50;
    }

    public void setTimeBetweenOrderCancel(int timeBetweenOrderCancel) {
        this.timeBetweenOrderCancel = timeBetweenOrderCancel;
    }

    public void setCancelAfterXOrders(int cancelAfterXOrders) {
        this.cancelAfterXOrders = cancelAfterXOrders;
    }

    @Override
    public void run() {
        counter++;
        orderId++;

        String orderMessage = null;
        logger.info(String.format("Generating a new order message with this id: %d.", orderId));
        try {
            orderMessage = orderCommand.generate(orderId);
            orderService.sendMessage(orderMessage);
            logger.debug(String.format("message content: %s", orderMessage));
        } catch (FormatException e) {
            logger.error("Something went wrong with formatting an object", e);
        } catch (CommunicationException e) {
            logger.error("Something went wrong while trying to send am order message", e);
        }
        if (counter == cancelAfterXOrders) {
            final int orderIdToCancel = orderId;

            Executors.newSingleThreadScheduledExecutor().schedule(() -> {
                logger.info(String.format("Generating a new cancel message with this id: %d.", orderIdToCancel));
                try {
                    String cancelMessage = cancelCommand.generate(orderIdToCancel);
                    cancelService.sendMessage(cancelMessage);
                    logger.debug(String.format("message content: %s", cancelMessage));
                } catch (FormatException e) {
                    logger.error("Something went wrong with formatting an object", e);
                } catch (CommunicationException e) {
                    logger.error("Something went wrong while trying to send am cancel message", e);
                }
            }, Integer.toUnsignedLong(timeBetweenOrderCancel), TimeUnit.MILLISECONDS);
            counter = 0;
        }
    }

    public void initServices() {
        try {
            orderService.init();
            cancelService.init();
            logger.info("Messafeservices have been initialized.");
        } catch (CommunicationException e) {
            logger.error("Something went wrong while initializing th messageservices", e);
        }
    }

    public void stop() {
        try {
            orderService.shutdown();
            cancelService.shutdown();
            logger.info("Messafeservices have been stopped.");
        } catch (CommunicationException e) {
            logger.error("Something went wrong while stopping the messageservices", e);
        }
    }
}
