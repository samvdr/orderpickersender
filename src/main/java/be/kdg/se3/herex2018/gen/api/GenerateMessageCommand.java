package be.kdg.se3.herex2018.gen.api;

import be.kdg.se3.herex2018.gen.exceptions.FormatException;

/**
 * An interface to be implemented by any class who will generate a message.
 */
public interface GenerateMessageCommand {
    /**
     * @param formatter formatter to translate the generated object to the desired format
     */
    void setFormatter(Formatter formatter);

    /**
     * Generates an object with the given order id, this ilso the command that needs to be executed.
     *
     * @param orderId a generated number starting from 1000000 to serve as an order id
     * @return a string of the object wrapped in the desired format
     * @throws FormatException
     */
    String generate(int orderId) throws FormatException;
}
