package be.kdg.se3.herex2018.gen.impl;

import be.kdg.se3.herex2018.gen.api.Formatter;
import be.kdg.se3.herex2018.gen.exceptions.FormatException;
import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class responsible for the formatting of an object to an XML string.
 */
public class XMLFormatter implements Formatter {
    private final Logger logger = LoggerFactory.getLogger(XMLFormatter.class);
    private final XStream xStream = new XStream();

    @Override
    public String wrap(Object o) throws FormatException {
        String message = null;

        logger.info("object is being wrapped");
        try {
            message = xStream.toXML(o);

            logger.debug(String.format("message content: %s", message));
        } catch (Exception e) {
            throw new FormatException("something went wrong while translating an object to an XML message", e);
        }
        return message;
    }
}
