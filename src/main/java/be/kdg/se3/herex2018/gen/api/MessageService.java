package be.kdg.se3.herex2018.gen.api;

import be.kdg.se3.herex2018.gen.exceptions.CommunicationException;

/**
 * A service to adapt a message broker or any other way to send a message.
 */
public interface MessageService {
    /**
     * Initializes the used server/tool used to send the messages with.
     *
     * @throws CommunicationException
     */
    void init() throws CommunicationException;

    /**
     * @param message string that will be send
     * @throws CommunicationException
     */
    void sendMessage(String message) throws CommunicationException;

    /**
     * The actions needed to close the used server/tool.
     *
     * @throws CommunicationException
     */
    void shutdown() throws CommunicationException;
}
