package be.kdg.se3.herex2018.gen;

import be.kdg.se3.herex2018.gen.api.Formatter;
import be.kdg.se3.herex2018.gen.api.GenerateMessageCommand;
import be.kdg.se3.herex2018.gen.api.MessageService;
import be.kdg.se3.herex2018.gen.impl.GenerateCancelMessage;
import be.kdg.se3.herex2018.gen.impl.GenerateOrderMessage;
import be.kdg.se3.herex2018.gen.impl.RabbitMQ;
import be.kdg.se3.herex2018.gen.impl.XMLFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Test class for the application.
 */
public class RunGen {
    public static void main(String[] args) {
        Formatter formatter = new XMLFormatter();
        MessageService orderService = new RabbitMQ("OrderMessages", "localhost");
        MessageService cancelService = new RabbitMQ("CancelMessages", "localhost");
        GenerateMessageCommand cancelCommand = new GenerateCancelMessage();
        GenerateMessageCommand orderCommand = new GenerateOrderMessage();

        ((GenerateCancelMessage) cancelCommand).setFormatter(formatter);
        ((GenerateOrderMessage) orderCommand).setFormatter(formatter);
        ((GenerateOrderMessage) orderCommand).setCustomerIds(getIds(15));
        ((GenerateOrderMessage) orderCommand).setProductIds(getIds(20));
        ((GenerateOrderMessage) orderCommand).setMaxProductItemAmount(5);
        ((GenerateOrderMessage) orderCommand).setMinProductItemAmount(1);
        ((GenerateOrderMessage) orderCommand).setMaxAmountOfProducts(15);
        ((GenerateOrderMessage) orderCommand).setMinAmountOfProducts(2);
        ((GenerateOrderMessage) orderCommand).setMaxPrice(1000);
        ((GenerateOrderMessage) orderCommand).setMinPrice(100);

        Generator generator = new Generator(orderService, cancelService, orderCommand, cancelCommand);

        generator.setCancelAfterXOrders(25);
        generator.setIntervalBetweenOrders(500);
        generator.setTimeBetweenOrderCancel(5000);

        generator.start();
    }

    private static List<Integer> getIds(int count) {
        ArrayList<Integer> ids = new ArrayList<>(count);
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 7; j++) {
                sb.append(random.nextInt(9));
            }
            ids.add(Integer.parseInt(sb.toString()));
        }
        return ids;
    }
}
